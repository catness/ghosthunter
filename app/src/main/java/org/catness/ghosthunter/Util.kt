package org.catness.ghosthunter

import android.graphics.Bitmap

// https://android--code.blogspot.com/2018/04/android-kotlin-resize-bitmap-keep.html
// Extension function to resize bitmap using new width value by keeping aspect ratio
fun Bitmap.resizeByWidth(width:Int):Bitmap{
    val ratio:Float = this.width.toFloat() / this.height.toFloat()
    val height:Int = Math.round(width / ratio)

    return Bitmap.createScaledBitmap(
            this,
            width,
            height,
            false
    )
}


// Extension function to resize bitmap using new height value by keeping aspect ratio
fun Bitmap.resizeByHeight(height:Int): Bitmap {
    val ratio:Float = this.height.toFloat() / this.width.toFloat()
    val width:Int = Math.round(height / ratio)

    return Bitmap.createScaledBitmap(
            this,
            width,
            height,
            false
    )
}
