package org.catness.ghosthunter

import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import kotlin.math.floor
import kotlin.random.Random

class CustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    private val LOG_TAG: String = this.javaClass.simpleName
    private val paint = Paint().apply {
        // Smooth out edges of what is drawn without affecting shape.
        isAntiAlias = true
        strokeWidth = resources.getDimension(R.dimen.strokeWidth)
        style = Paint.Style.FILL_AND_STROKE
    }
    private val path = Path()

    private val clipRectRight = resources.getDimension(R.dimen.clipRectRight)
    private val clipRectBottom = resources.getDimension(R.dimen.clipRectBottom)
    private val clipRectTop = resources.getDimension(R.dimen.clipRectTop)
    private val clipRectLeft = resources.getDimension(R.dimen.clipRectLeft)
    private val clipRectWidth = clipRectRight - clipRectLeft
    private val clipRectHeight = clipRectBottom - clipRectTop
    private val frameWidth = resources.getDimension(R.dimen.frameWidth)

    private val colorBackground = Color.parseColor("#0000a0")

    private var shouldDrawSpotLight = false
    private var shouldDrawGhost = false

    private val bitmapRoom1 = BitmapFactory.decodeResource(
        resources,
        R.drawable.room1
    ).resizeByWidth(clipRectWidth.toInt())

    private val bitmapRoom2 = BitmapFactory.decodeResource(
        resources,
        R.drawable.room2
    ).resizeByWidth(clipRectWidth.toInt())

    private val bitmapRoom3 = BitmapFactory.decodeResource(
        resources,
        R.drawable.room3
    ).resizeByWidth(clipRectWidth.toInt())

    private val bitmapRoom4 = BitmapFactory.decodeResource(
        resources,
        R.drawable.room4
    ).resizeByWidth(clipRectWidth.toInt())


    private val spotlight = BitmapFactory.decodeResource(resources, R.drawable.mask)
    private val ghostScale = spotlight.width / 2
    private val bitmapGhost1 = BitmapFactory.decodeResource(
        resources,
        R.drawable.ghost1
    ).resizeByWidth(ghostScale)

    private val bitmapGhost2 = BitmapFactory.decodeResource(
        resources,
        R.drawable.ghost2
    ).resizeByWidth(ghostScale)

    private val bitmapGhost3 = BitmapFactory.decodeResource(
        resources,
        R.drawable.ghost3
    ).resizeByWidth(ghostScale)

    private val bitmapGhost4 = BitmapFactory.decodeResource(
        resources,
        R.drawable.ghost4
    ).resizeByWidth(ghostScale)

    private val bitmapGhost5 = BitmapFactory.decodeResource(
        resources,
        R.drawable.ghost5
    ).resizeByWidth(ghostScale)

    private val bitmapGhost6 = BitmapFactory.decodeResource(
        resources,
        R.drawable.ghost6
    ).resizeByWidth(ghostScale)


    private val ghosts: List<Bitmap> = listOf(bitmapGhost1, bitmapGhost2, bitmapGhost3,
        bitmapGhost4, bitmapGhost5, bitmapGhost6)
    private var currentGhostNum = 0
    private var currentGhost: Bitmap = ghosts[currentGhostNum]

    private var ghostX = 0f
    private var ghostY = 0f
    private lateinit var ghostRect: RectF

    private var shader: Shader
    private val shaderMatrix = Matrix()

    init {
        val bitmap = Bitmap.createBitmap(spotlight.width, spotlight.height, Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bitmap)
        val shaderPaint = Paint(Paint.ANTI_ALIAS_FLAG)

        // Draw a background rectangle
        shaderPaint.color = Color.argb(0xb0, 0, 0, 0xff)
        canvas.drawRect(
            0.0f,
            0.0f,
            spotlight.width.toFloat(),
            spotlight.height.toFloat(),
            shaderPaint
        )
        //shaderPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
        shaderPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.LIGHTEN)
        canvas.drawBitmap(spotlight, 0.0f, 0.0f, shaderPaint)
        shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)

        var filter: ColorFilter = LightingColorFilter(Color.WHITE, Color.RED)
        shaderPaint.colorFilter = filter
        paint.shader = shader
    }

    private fun setGhostPosition() {
        ghostX = floor(Random.nextFloat() * (width - currentGhost.width))
        ghostY = floor(Random.nextFloat() * (height - currentGhost.height))

        ghostRect = RectF(
            (ghostX),
            (ghostY),
            (ghostX + currentGhost.width),
            (ghostY + currentGhost.height)
        )
    }

    private fun nextGhost() {
        if (++currentGhostNum >= ghosts.size) currentGhostNum = 0
        currentGhost = ghosts[currentGhostNum]
        setGhostPosition()
    }

    private fun drawRoomInterior(canvas: Canvas, bitmap: Bitmap) {
        canvas.clipRect(
            clipRectLeft, clipRectTop,
            clipRectRight, clipRectBottom
        )

        canvas.drawBitmap(
            bitmap,
            clipRectLeft,
            clipRectTop,
            paint
        )

    }

// Sorry, too lazy to make 2 versions of clipping rectangle...
// the older one is canvas.ClipRect with an additional parameter Region.Op.DIFFERENCE
    @RequiresApi(Build.VERSION_CODES.O)
    private fun drawWindow(canvas: Canvas, x: Float, y: Float, bitmap: Bitmap) {
// window with a circular top

        canvas.save()
        canvas.translate(x, y)
        path.reset()
// not sure about the algorithm of fillType, but it doesn't work with EVEN_ODD
// (clips out a half-circle below the circle top)
        path.fillType = Path.FillType.WINDING

    // these rectangles are for window frames
        canvas.clipOutRect(
            clipRectLeft, clipRectHeight / 2 - frameWidth / 2,
            clipRectRight,
            clipRectHeight / 2 + frameWidth / 2
        )

        canvas.clipOutRect(
            clipRectLeft,
            clipRectHeight * 3 / 4 - frameWidth / 2,
            clipRectRight,
            clipRectHeight * 3 / 4 + frameWidth / 2
        )

        canvas.clipOutRect(
            clipRectLeft,
            clipRectHeight / 4 - frameWidth / 2,
            clipRectRight,
            clipRectHeight / 4 + frameWidth / 2
        )

        canvas.clipOutRect(
            clipRectWidth / 2 - frameWidth / 2,
            clipRectTop,
            clipRectWidth / 2 + frameWidth / 2,
            clipRectBottom
        )

        var circleRadius = clipRectWidth / 2

        path.addCircle(
            clipRectLeft + circleRadius,
            clipRectTop + circleRadius,
            circleRadius, Path.Direction.CCW
        )
        path.addRect(
            clipRectLeft,
            clipRectHeight / 4,
            clipRectRight,
            clipRectBottom,
            Path.Direction.CCW
        )

        canvas.clipPath(path)

        drawRoomInterior(canvas, bitmap)
        canvas.restore()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun drawWindow1(canvas: Canvas, x: Float, y: Float, bitmap: Bitmap) {
// window with a triangular top
        canvas.save()
        canvas.translate(x, y)
        path.reset()

        // window frames
        canvas.clipOutRect(
            clipRectLeft, clipRectHeight / 3 - frameWidth / 2,
            clipRectRight,
            clipRectHeight / 3 + frameWidth / 2
        )

        canvas.clipOutRect(
            clipRectLeft, clipRectHeight * 2 / 3 - frameWidth / 2,
            clipRectRight,
            clipRectHeight * 2 / 3 + frameWidth / 2
        )

        canvas.clipOutRect(
            clipRectLeft, clipRectHeight / 6 - frameWidth - frameWidth / 2,
            clipRectRight,
            clipRectHeight / 6 - frameWidth + frameWidth / 2
        )

        val off = frameWidth
        canvas.clipOutRect(
            clipRectWidth / 3 - off - frameWidth / 2,
            clipRectTop,
            clipRectWidth / 3 - off + frameWidth / 2,
            clipRectBottom
        )
        canvas.clipOutRect(
            clipRectWidth * 2 / 3 + off - frameWidth / 2,
            clipRectTop,
            clipRectWidth * 2 / 3 + off + frameWidth / 2,
            clipRectBottom
        )

        path.fillType = Path.FillType.EVEN_ODD
        //draw a triangle
        path.moveTo(clipRectLeft, clipRectHeight / 4)
        path.lineTo(clipRectWidth / 2, clipRectTop)
        path.lineTo(clipRectRight, clipRectBottom / 4)
        path.close()

        path.addRect(
            clipRectLeft,
            clipRectBottom / 4,
            clipRectRight,
            clipRectBottom,
            Path.Direction.CCW
        )
        canvas.clipPath(path)
        drawRoomInterior(canvas, bitmap)
        canvas.restore()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawColor(colorBackground)

        var x1 = (width / 2 - clipRectWidth) / 2.toFloat()
        var ym = height / 2
        //Log.i(LOG_TAG, "ym=$ym")
        var y = (ym + (ym - clipRectHeight) / 2)
        drawWindow(canvas, x1, y, bitmapRoom1)

        var x2 = (width / 2 + clipRectWidth / 2)

        // Log.i(LOG_TAG, "x=$x1,$x2 y=$y")
        drawWindow(canvas, x2, y, bitmapRoom2)

        y = (ym - clipRectHeight) / 2
        // Log.i(LOG_TAG, "*** x=$x1,$x2 y=$y ")
        drawWindow1(canvas, x1, y, bitmapRoom3)
        drawWindow1(canvas, x2, y, bitmapRoom4)

        if (shouldDrawSpotLight) {
            canvas.drawRect(0.0f, 0.0f, width.toFloat(), height.toFloat(), paint)

            if (shouldDrawGhost) {
                canvas.drawBitmap(currentGhost, ghostX, ghostY, paint)
            }

        } else {
            // adding color makes it too bleak...
            //canvas.drawColor(Color.parseColor("#ccb0b0ff"))
        }
    }

    override fun onSizeChanged(
        newWidth: Int,
        newHeight: Int,
        oldWidth: Int,
        oldHeight: Int
    ) {
        super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight)
        Log.i(
            LOG_TAG,
            "width=$width height=$height " +
                    "cliprecWidth=$clipRectWidth cliprecHeight=$clipRectHeight " +
                    "spotlightwidth = ${spotlight.width}"

        )
        setGhostPosition()
    }

    override fun onTouchEvent(motionEvent: MotionEvent): Boolean {
        val motionEventX = motionEvent.x
        val motionEventY = motionEvent.y

        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                shouldDrawSpotLight = true
                shouldDrawGhost = ghostRect.contains(motionEventX, motionEventY)

            }
            MotionEvent.ACTION_UP -> {
                shouldDrawSpotLight = false
                nextGhost()
            }
            MotionEvent.ACTION_MOVE -> {
                shouldDrawGhost = ghostRect.contains(motionEventX, motionEventY)
            }
        }

        shaderMatrix.setTranslate(
            motionEventX - spotlight.width / 2.0f,
            motionEventY - spotlight.height / 2.0f
        )
        shader.setLocalMatrix(shaderMatrix)
        invalidate()

        return true
    }


}