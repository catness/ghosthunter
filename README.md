# About the project

**Ghosthunter** is an example for [Advanced Android in Kotlin 02.2: Clipping canvas objects](https://codelabs.developers.google.com/codelabs/advanced-android-kotlin-training-clipping-canvas-objects) and [02.3: Creating effects with shaders](https://codelabs.developers.google.com/codelabs/advanced-android-kotlin-training-shaders). It's a game concept demo where you have to find ghosts in a haunted house using a special ghost-detector spotlight. The house (or rather, the house windows) is an example of clipping (2 different kinds: clip the shapes out of the image, and clip the image boundary), and the spotlight (copied almost entirely from the 2nd lab, with a few changes) is an example of shading.

Images of the house rooms and ghosts are from Creazilla.com:

* https://creazilla.com/nodes/35474-bedroom-clipart
* https://creazilla.com/nodes/33345-interior-room-clipart
* https://creazilla.com/nodes/20912-ghost-clipart
* https://creazilla.com/nodes/26935-halloween-ghost-clipart
* https://creazilla.com/nodes/29525-ghost-with-a-cellephone-clipart
* https://creazilla.com/nodes/5503-ghost-clipart
* https://creazilla.com/nodes/4111-cartoon-ghost-clipart

The apk is here: [ghosthunter.apk](apk/ghosthunter.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
